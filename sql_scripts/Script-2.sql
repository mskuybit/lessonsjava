CREATE TABLE Supplies_store(
id INT AUTO_INCREMENT,
product VARCHAR(255),
exparation VARCHAR(255) ,
vendor VARCHAR(255),
breakable VARCHAR(100),
price VARCHAR(255),
PRIMARY KEY (id)
);

CREATE TABLE Vendor(
id INT AUTO_INCREMENT,
company VARCHAR(255),
country VARCHAR(100),
PRIMARY KEY (id)
);

CREATE DATABASE Products;
use Products;


INSERT INTO Supplies_store(product, exparation, vendor, breakable, price)VALUES
("phone",2017,"Apple","yes",600),
("cord", 2016,"Nexus","no",10),
("cover",2015,"Moto","yes",30),
("charger",2014,"Lenovo","yes",50);

INSERT INTO Vendor(company, country)VALUES
("Apple", "USA"),
("Nexus", "USA"),
("Moto", "China"),
("Lenovo", "China");

SELECT * FROM Supplies_store WHERE vendor="Apple" AND breakable="yes";

SELECT * FROM Supplies_store WHERE exparation BETWEEN 2015 AND 2016;

SELECT * FROM Supplies_store WHERE price BETWEEN 10 and 50;

SELECT * FROM Supplies_store WHERE product LIKE "p%";

SELECT * FROM Supplies_store WHERE product LIKE "co%";

SELECT * FROM Supplies_store ORDER BY product DESC;

SELECT * FROM Supplies_store ORDER BY product ASC;

SELECT * FROM Vendor WHERE id BETWEEN 1 and 2;

SELECT * FROM Supplies_store;

UPDATE Supplies_store SET
product_name = CONCAT(UPPER(LEFT(product_name, 1)), LOWER(SUBSTRING(product_name, 2)));

















