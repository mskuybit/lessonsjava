CREATE DATABASE learnsql; 

use learnsql;

CREATE TABLE Human(
id INT AUTO_INCREMENT,
first_name VARCHAR(20) NULL DEFAULT 'Marina',
last_name VARCHAR(20),
age INT,
CONSTRAINT UNIQUE KEY `name` (first_name, last_name),
PRIMARY KEY (id)
);

DROP TABLE Human;

/*Add column*/

ALTER TABLE Human
ADD address_name VARCHAR(40);

ALTER TABLE Human
ADD address_name_mod VARCHAR(40) DEFAULT "some_address";

ALTER TABLE Human
DROP COLUMN address_name;

ALTER TABLE Human
MODIFY COLUMN first_name VARCHAR(100);

ALTER TABLE Employee AUTO_INCREMENT = 1;

CREATE TABLE Employee(
id INT AUTO_INCREMENT,
name VARCHAR(20),
human_id INT,
CONSTRAINT FOREIGN KEY (human_id) REFERENCES Human(id),
PRIMARY KEY(id)
);

INSERT INTO Employee(name, human_id) VALUES
("BobE",  88);

SELECT * FROM Human;

SELECT * FROM Human WHERE id IN(SELECT em.human_id FROM Employee em);

SELECT * FROM Employee;

INSERT INTO Human(first_name, last_name, age) VALUES
("Bob",  "Smith", 10),
("Bob1", "Smith1", 15),
("Bob",  "Smith5", 50),
("Bob3", "Smith3", 60),
("Bob4", "Smith4", 30);

INSERT INTO Human(last_name, age) VALUES
("Smith", 10),
("Smith1", 15);

SELECT * FROM Human WHERE age > 10 AND age < 60;

SELECT * FROM Human WHERE age = 10 OR age = 60;

SELECT DISTINCT age FROM Human;

SELECT * FROM Human ORDER BY age DESC;

SELECT * FROM Human h WHERE h.last_name LIKE 'Sm%';

SELECT * FROM Human WHERE last_name LIKE '%4';

SELECT * FROM Human WHERE last_name LIKE 'S%t%'; 

UPDATE Human SET first_name = 'Otherzzz',
last_name = 'avalev' WHERE first_name = 'Other';

SELECT * FROM Human LIMIT 3;

DELETE FROM Human WHERE id = 1;

SELECT * FROM Human WHERE age ANY(15, 50);

SELECT * FROM Human WHERE age BETWEEN 15 AND 50;

SELECT sum(id) count_of_rows FROM Human;

SELECT now();
SELECT CURRENT_DATE;
SELECT CURRENT_TIME;


SELECT date_add(NOW(), INTERVAL 2 HOUR);
SELECT date_add(NOW(), INTERVAL 2 MONTH);


/*DATE_FORMAT*/ 

SELECT DATE_FORMAT(NOW(),'%b %d %Y %h:%i %p');
SELECT DATE_FORMAT(NOW(),'%m-%d-%Y');
SELECT DATE_FORMAT(NOW(),'%d %b %y');
SELECT DATE_FORMAT(NOW(),'%d %b %Y %T:%f');

/*Case*/

SELECT first_name, last_name, (CASE
WHEN age = 15 THEN 'man is 15'
WHEN age = 30 THEN 'man is 30'
WHEN age = 50 THEN 'man is 50'
WHEN age = 60 THEN 'man is 60'
END
) FROM Human;


/*Group by*/

CREATE TABLE address(
address_id INT NOT NULL AUTO_INCREMENT,
city VARCHAR(50) DEFAULT NULL,
street VARCHAR(50) DEFAULT NULL,
number_street tinyint,
count_of_citizens INT DEFAULT 0,
PRIMARY KEY (address_id));

INSERT INTO address(city, street, number_street, count_of_citizens) VALUES
("Kiev",            "Bankova",    5,  100),
("Dnipropetrovsk",  "Plehanova",  10, 300),
("Lviv",            "Gorodocka",  2,  30),
("Dnipropetrovsk",  "Plehanova",  5,  45),
("Lviv",            "Gorodocka",  3,  5), 
("Kirovograd", 		"Popova",     30, 45),
("Kirovograd", 		"LermONtova", 3,  78),
("Lviv", 		    "Gorodocka",  10, 41),
("Kirovograd", 		"Popova",     0,  42);

SELECT COUNT(*) counter FROM address WHERE city = 'Lviv';

SELECT s.city, s.street, SUM(s.count_of_citizens) 
FROM address s
GROUP BY s.city, s.street;

SELECT s.city, SUM(s.count_of_citizens) 
FROM address s
GROUP BY s.city
HAVING SUM(s.count_of_citizens) > 100;