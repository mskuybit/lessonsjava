CREATE DATABASE Enterprise;

USE Enterprise;

ALTER TABLE Employee AUTO_INCREMENT = 1;

CREATE TABLE Employee(
employee_id INT AUTO_INCREMENT,
first_name VARCHAR(50) DEFAULT NULL,
last_name VARCHAR(50) DEFAULT NULL,
department_id INT, 
CONSTRAINT FOREIGN KEY (department_id) REFERENCES Department(department_id),
PRIMARY KEY(employee_id));

ALTER TABLE Department AUTO_INCREMENT = 1;

CREATE TABLE Department(
department_id INT AUTO_INCREMENT,
city VARCHAR(50) DEFAULT NULL,
PRIMARY KEY(department_id)
);

INSERT INTO Department(department_id, city) VALUES
(1, "Aberdeen"),
(2, "Matawan"),
(3, "Marlboro"),
(4, "Clifwood"),
(5, "Holmdel");

INSERT INTO Employee(first_name, last_name, department_id) VALUES
('Marina', 'Skuybit',  1),
('Marin',  'Skuybit',  2),
('Marin2', 'Skuybi',   3),
('Mari',   'Skuy',     4),
('Mar',    'Sku',      5),
('Marin5', 'Skuyb5',   1),
('Ma',     'Sk',       2),
('Ma7',    'Sit7',     3),
('Maa8',   'Sybit8',   4),
('Mina9',  'Sk1',      5),
('Mar12',  'Skuybit0', 1);

SELECT * FROM Department;
SELECT * FROM Employee;

SELECT city FROM Department;

SELECT * FROM Employee ORDER BY first_name ASC;

SELECT COUNT(*) FROM Employee WHERE department_id = 2; 

SELECT department_id, COUNT(*) FROM Employee GROUP BY department_id;

SELECT COUNT(*), department_id FROM Employee GROUP BY department_id ORDER BY COUNT(*) DESC;

SELECT e.first_name, e.last_name, e.department_id, d.department_id 
FROM Employee e, Department d
WHERE e.department_id = d.department_id
ORDER BY d.city;

SELECT d.city, d.department_id, COUNT(*) count_employees 
FROM Employee e INNER JOIN
Department d
ON e.department_id = d.department_id
GROUP BY d.department_id
ORDER BY d.city;

SELECT * FROM Department WHERE city LIKE 'M%';

SELECT e.first_name, e.last_name, d.department_id 
FROM Employee e , Department d
WHERE e.department_id = d.department_id AND d.city LIKE '%O%';

SELECT d.city, COUNT(e.employee_id) count_of_employee
FROM Employee e 
INNER JOIN Department d
ON e.department_id = d.department_id
WHERE d.city LIKE '%O%' AND d.city IS NOT NULL
GROUP BY d.city;


SELECT last_name FROM Employee
GROUP BY last_name
HAVING COUNT(*) > 1
ORDER BY last_name;

SELECT last_name, first_name FROM Employee, Department
WHERE city = 'Matawan'
GROUP BY last_name, first_name 
HAVING COUNT(last_name) > 1
ORDER BY first_name;

SELECT last_name, first_name FROM Employee e
INNER JOIN Department d
ON d.department_id = e.department_id
WHERE city = 'Matawan'
GROUP BY last_name, first_name 
HAVING COUNT(last_name) > 1
ORDER BY first_name;

INSERT INTO Employee(first_name, last_name, department_id) VALUES
('Marina', 'Skuybit',  3);

SELECT d.department_id, d.city, COUNT(first_name) FROM Department d, Employee e
WHERE e.department_id = d.department_id AND first_name = 'Marina'
GROUP BY d.city, d.department_id
HAVING COUNT(first_name) > 1
ORDER BY d.department_id;


SELECT  d.department_id, d.city, COUNT(first_name) 
FROM Department d
INNER JOIN Employee e
ON e.department_id = d.department_id 
WHERE first_name = 'Marina'
ORDER BY d.department_id
GROUP BY d.city
HAVING COUNT(first_name) > 1;


INSERT INTO Department(department_id, city) VALUES
(6, "Edison");

INSERT INTO Employee(first_name, last_name, department_id) VALUES
('M', 'SNULL',  NULL);