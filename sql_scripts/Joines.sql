SELECT h.first_name, h.last_name, em.name 
FROM Human h
INNER JOIN Employee em
ON h.id = em.human_id WHERE first_name = 'Bob';

SELECT h.first_name, h.last_name, em.name 
FROM Human h
LEFT OUTER JOIN Employee em
ON h.id = em.human_id;

SELECT * FROM Human 
CROSS JOIN Employee;

