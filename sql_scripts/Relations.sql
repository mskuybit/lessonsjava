/*https://stackoverflow.com/questions/7296846/how-to-implement-one-to-one-one-to-many-and-many-to-many-relationships-while-de*/

/*One to one*/

USE forlearning;

CREATE TABLE student(
student_id INT AUTO_INCREMENT,
first_name VARCHAR(50) DEFAULT NULL,
last_name VARCHAR(50) DEFAULT NULL,
class_student_id INT NULL,
FOREIGN KEY (class_student_id) REFERENCES class_student(class_id),
PRIMARY KEY (student_id),
UNIQUE KEY `student_class` (first_name, last_name, class_student_id)
);

CREATE TABLE class_student(# you can have a "link back" if you need
class_id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(50) DEFAULT NULL,
PRIMARY KEY (class_id)
);

ALTER TABLE class_student AUTO_INCREMENT = 1;

INSERT INTO class_student(name) VALUES("Mathematics");
INSERT INTO class_student(name) VALUES("Chemistry");
INSERT INTO class_student(name) VALUES("Drawing");

ALTER TABLE student AUTO_INCREMENT = 1;

INSERT INTO student(first_name, last_name, class_student_id) VALUES("Linus", "Torvalds", 2);
INSERT INTO student(first_name, last_name, class_student_id) VALUES("Mike", "Tutor", 1);
INSERT INTO student(first_name, last_name, class_student_id) VALUES("Jim", "Kerry", 3);
INSERT INTO student(first_name, last_name, class_student_id) VALUES("Jim", "Kerry", 3);# there will be error: duplicate

TRUNCATE TABLE student;
DROP TABLE student;
C

SELECT * FROM student;
SELECT * FROM class_student;


/*One to many*/

ALTER TABLE teacher AUTO_INCREMENT = 1;

CREATE TABLE teacher(
teacher_id INT AUTO_INCREMENT,
first_name VARCHAR(50) DEFAULT NULL,
last_name VARCHAR(50) DEFAULT NULL,
PRIMARY KEY (teacher_id)
);

DROP TABLE IF EXISTS teacher;
DROP TABLE IF EXISTS teacher_class;

ALTER TABLE teacher_class AUTO_INCREMENT = 1;

CREATE TABLE teacher_class(
teacher_class_id INT AUTO_INCREMENT,
name VARCHAR(50) DEFAULT NULL,
teacher_id INT NULL,
PRIMARY KEY (teacher_class_id),
FOREIGN KEY (teacher_id) REFERENCES teacher(teacher_id)
ON DELETE CASCADE
);

INSERT INTO teacher(first_name, last_name) VALUES("Linus", "Torvalds");
INSERT INTO teacher(first_name, last_name) VALUES("Mike", "Tutor");


INSERT INTO teacher_class(name, teacher_id) VALUES("Chemistry", 1);
INSERT INTO teacher_class(name, teacher_id) VALUES("Drawing", 1);

INSERT INTO teacher_class(name, teacher_id) VALUES("History", 2);
INSERT INTO teacher_class(name, teacher_id) VALUES("Mathematic", 2);


SELECT * FROM teacher;
SELECT * FROM teacher_class;

TRUNCATE TABLE teacher_class;

/*Test ON DELETE CASCADE*/

DELETE FROM teacher WHERE teacher_id = 1;


/*Many-to-many*/

ALTER TABLE citizen AUTO_INCREMENT = 1;

CREATE TABLE citizen(
citizen_id INT AUTO_INCREMENT,
first_name VARCHAR(50) DEFAULT NULL,
last_name VARCHAR(50) DEFAULT NULL,
PRIMARY KEY (citizen_id));

INSERT INTO citizen(first_name, last_name) VALUES("Linus", "Torvalds");
INSERT INTO citizen(first_name, last_name) VALUES("Mark", "Avrelliy");

ALTER TABLE city AUTO_INCREMENT = 1;

CREATE TABLE city(
city_id INT AUTO_INCREMENT,
name VARCHAR(50) DEFAULT NULL,
PRIMARY KEY (city_id));

INSERT INTO city(name) VALUES("Dnepr");
INSERT INTO city(name) VALUES("Kiev");


CREATE TABLE city_citizen(
city_id INT,
citizen_id INT,
PRIMARY KEY (city_id, citizen_id),
FOREIGN KEY (city_id) REFERENCES city(city_id),
FOREIGN KEY (citizen_id) REFERENCES citizen(citizen_id));

DROP TABLE city_citizen;

SELECT * FROM citizen;
SELECT * FROM city;
SELECT * FROM city_citizen;

INSERT INTO city_citizen(city_id, citizen_id) VALUES(1, 1);
INSERT INTO city_citizen(city_id, citizen_id) VALUES(2, 1);

INSERT INTO city_citizen(city_id, citizen_id) VALUES(1, 2);
INSERT INTO city_citizen(city_id, citizen_id) VALUES(2, 2);

