

Имеются сущности с полями:

Студент(имя, фамилия, возраст, пол, дата рождения, адрес, название университета, название факультета, номер группы);
Университет(название, дата основания, количество факультетов);
Факультет(название, дата основания, количество групп);
Группа(название, дата основания, количество студентов, староста, факультет);
Адрес(город, улица, номер дома, номер квартиры);

Создать 20 студентов так, чтобы некоторые имели:
- общие университеты.
- факультеты.
- номер группы.
- номера домов на одной улице.
- одну улицу.
Вывести в консоль:
 - всех студентов учащихся в одном университете(по названию университета).
 - всех студентов учащихся в одном университете(по названию университета) и на одном факультете.
 - количество студентов учащихся в каждом университете, факультете, группе, задавая/не задавая университета, факультета, группы.
 - найти группу с максимальным/минимальным количеством студентов.
 - подсчитать количество студентов, которые живут по одному адресу(улица, дом).
 - вывести даты рождения студентов в формате '30 Oct 16'.
 - вывести среднее количество людей в группе по университету.
 - вывести всех старост групп.
 - вывести университет, с наибольшим количеством факультетов, групп, студентов.
 
 В написании запросов использовать:
- первичные и внешние ключи, 
- autoincrement,
- not null/null,
- соответствующие типы данных,
- GROUP BY

/*RelationShip:
- Student - Address - One-to-One
- Student - Class - Many-to-One
- Faculty - Class - One-to-Many
- University - Faculty - Many-to-Many*/
USE Study_community;

ALTER TABLE Student AUTO_INCREMENT = 1;

CREATE TABLE student(
student_id INT AUTO_INCREMENT,
name_of_student VARCHAR (50) NOT NULL, 
surname  VARCHAR (50) NOT NULL,
age TINYINT NOT NULL,
gender ENUM ('male', 'female'),
date_of_birthday DATE NOT NULL,
address_id INT NULL,
university_id INT NULL,
faculty_id INT NULL,
class_id INT NULL,
PRIMARY KEY (student_id),
FOREIGN KEY (address_id) REFERENCES address (address_id),
FOREIGN KEY (university_id) REFERENCES university (university_id),
FOREIGN KEY (faculty_id) REFERENCES faculty (faculty_id),
FOREIGN KEY (class_id) REFERENCES class (class_id),
UNIQUE KEY `unique_student` (name_of_student, surname, class_id),
UNIQUE KEY `unique_student_address` (name_of_student, surname, address_id)
);

CREATE TABLE address(
address_id INT AUTO_INCREMENT,
city VARCHAR (50) NOT NULL,
street VARCHAR (50) NOT NULL,
building_number INT NOT NULL,
apartment_number INT NOT NULL,
PRIMARY KEY (address_id)
);


CREATE TABLE University(
university_id INT AUTO_INCREMENT,
university_name INT NOT NULL,
foundation_date DATE,
faculty_quantity INT,
PRIMARY KEY (university_id));


DROP TABLE University;
DROP IF EXISTS TABLE Faculty;


CREATE TABLE Faculty(
faculty_id INT AUTO_INCREMENT,
faculty_name VARCHAR(255),
foundation_date DATE,
group_quantity INT,
PRIMARY KEY (faculty_id));

CREATE TABLE University_faculty(
university_id INT NULL,
faculty_id INT NULL,
FOREIGN KEY (university_id) REFERENCES University (university_id),
FOREIGN KEY (faculty_id) REFERENCES Faculty (faculty_id)
);


CREATE TABLE Class(
class_id INT AUTO_INCREMENT,
group_name VARCHAR(255),
foundation_date DATE,
students_quantity INT,
head_of_the_group VARCHAR(255),/*as option add foreign key on student table*/
faculty_id INT NULL,
PRIMARY KEY (class_id),
FOREIGN KEY (faculty_id) REFERENCES Faculty(faculty_id)
);
