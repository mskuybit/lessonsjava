package lessons.first_lesson;

/**
 * Created by mskubit on 6/13/17.
 */
public class Count {
    public static void main(String[] args) {
        int a = 3;
        int b = 5;
        int sum = a + b;
        //System.out.println(sum);

        int f = 2;
        int r = 7;
        int sub = r - f;
        //System.out.println(sub);

        int p = 9;
        int g = 7;
        int mult = p * g;
        //System.out.println(mult);

        int h = 8;
        int s = 4;
        int div = h / s;
        //System.out.println(div);

        int m = 35;
        int y = 10;
        int mod = m % y;
        //System.out.println(mod);

        int count = 1;
        int x = ++count + --count - count-- + count++;// 2 + 1 - 1 + 0
        //System.out.println(x);

        int k = ++count + count++ - count-- + --count + --count - ++count;//2 + 2 - 1 + 0 + 0 - 2
        System.out.println(k);





    }
}
