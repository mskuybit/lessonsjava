package lessons.first_lesson;

/**
 * Created by mskubit on 6/10/17.
 */
public class FirstLesson {
    public static void main(String[] args) {
        //System.out.println("Hello");
        //type_of_variable name_of_variable = value;
        byte countOfPeople = 127;
        short s = 456;
        int i = 4655678;
        long l = 45678913L;

        int a = 10;
        int b = 5;
        int c = a + b;



        /*
        byte b = (byte)(countOfPeople + 10);//13

        int y = countOfPeople;
        */

        //System.out.println(b);

        int g = 120;

        float f = 12.5F;

        double d = 45.7;

        boolean j = true;

        boolean k = false;


        char c5 = '\u03ea';

        char c1 = 456;

        char c2 = 'F';


        //System.out.println(5/2.0);

        System.out.println(5 % 2);

    }
}

/*
*
* 51(10) -> 110011(2)
* 51/2 = 25, остаток 1
* 25/2 = 12, остаток 1
* 12/2 = 6, остаток 0
* 6/2 = 3, остаток 0
* 3/2 = 1, остаток 1
* */

/*
*
* 1235(10) -> 10011010011(2)
* 1235/2 = 617, balance 1
* 617/2 = 308, balance 1
* 308/2 = 154, balance 0
* 154/2 = 77, balance 0
* 77/2 = 38, balance 1
* 38/2 = 19, balance 0
* 19/2 = 9, balance 1
* 9/2 = 4, balance 1
* 4/2 = 2, balance 0
* 2/2 = 1, balance 0
* */
/*
*
* 45(10) -> 101101(2)
* 45/2 = 22, balance 1
* 22/2 = 11, balance 0
* 11/2 = 5, balance 1
* 5/2 = 2, balance 1
* 2/1 = 1, balance 0
* */
/*
*
* 89(10) -> 1011001(2)
* 89/2 = 44, -> balance 1
* 44/2 = 22, balance 0
* 22/2 = 11, balance 0
* 11/2 = 5, balance 1
* 5/2 = 2, balance 1
* 2/1 = 1, balance 0

*/

/*
*
* 225/8 = 28, balance 1
* 28/8 = 3, balance 4
*
*
* */

/*
* 1235(10) -> 2323(8)
* 1235/8 = 154, balance 3
* 154/8 = 19, balance 2
* 19/8 = 2, balance 3
* */
/*
* 1235(10) -> 4D3(16)
* 1235/16 = 77, balance 3
* 77/16 = 4, balance 13 = D
* */

/*
* 457(10) ->  711(8)
* 457/8 = 57, balance 1
* 57/8 = 7, balance 1
* */

/* 457(10) -> 1C9(16)
* 457/16 = 28, balance 9
* 28/16 = 1, balance 12 = C
* */

/* 89(10) ->  131(8)
* 89/8 = 11, balance 1
* 11/8 = 1, balance 3
* */

/* 89(10) ->  59(16)
* 89/16 = 5, balance 9
* */






