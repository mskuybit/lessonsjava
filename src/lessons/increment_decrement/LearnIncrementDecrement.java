package lessons.increment_decrement;

/**
 * Created by mskubit on 6/11/17.
 */
public class LearnIncrementDecrement {
    public static void main(String[] args) {

        int count = 2;
        //count = count + 1;
        //count += 1;

        //count++;//postfix form of increment

        //++count;//prefix form increment

        /*System.out.println(count++);
        System.out.println(count);*/

        //System.out.println(++count);

        int y = ++count + count++ + count++;//3 + 3 + 4

        System.out.println(y);


    }
}
