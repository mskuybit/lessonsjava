package lessons.learn_class;

/**
 * Created by mskubit on 7/15/17.
 */
public class TestLearnClass {
    public static void main(String[] args){
        //access_modificator type_of_reference name_of_reference = new type_of_reference();
        LearnClass aClass1 = new LearnClass(-10, (short)3);
        /*aClass1.age = -12;
        aClass1.countOfPeople = 15;*/
        aClass1.setAge(56);

        int age = aClass1.getAge();

        System.out.println(age);

        int r = 78;
        int y = 15;
        r = y;

        /*LearnClass aClass2 = new LearnClass();

        aClass2.age = 14;
        aClass2.countOfPeople = 25;*/
    }
}
