package lessons.learn_class;

/**
 * Создать класс калькулятор, создать методы для мат. операций(+, -, *, /).
 * Вызывать методы класса с разными значениями и выводить результат в консоль.
 */
public class Calculator {

    public int plus(int valueOne, int valueTwo) {
        int sum = valueOne + valueTwo;
        return sum;
    }

    public int minus(int valueOne, int valueTwo) {
        return valueOne - valueTwo;
    }

    public int mod(int valueOne, int valueTwo) {
        return valueOne * valueTwo;
    }

    public int div(int valueOne, int valueTwo) {
        return valueOne / valueTwo;
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        int sum = calculator.plus(103, 8);

        System.out.println("Sum " + sum);

        int minus = calculator.minus(45, 32);

        System.out.println("Minus " + minus);

        int mod = calculator.mod(33, 2);

        System.out.println("Mod" + mod);

        int div = calculator.div(10, 2);

        System.out.println("Div" + div);
    }
}
