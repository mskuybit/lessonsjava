package lessons.learn_class.human;

public class Human {
    private int age;//0
    private int countOfChildren;//0

    public Human(int newAge, int newCountOfChildren) {
        age =newAge;
        countOfChildren = newCountOfChildren;
    }


    public void setAge(int newAge) {
        age = newAge;
    }

    public void setCountOfChildren(int newCountOfChildren) {
        countOfChildren = newCountOfChildren;
    }

    public static void main(String[] args) {
        Human human = new Human(32, 3);
        human.setAge(56);
        human.setCountOfChildren(2);
    }
}
