package lessons.learn_class;

/**
 * Created by mskubit on 7/16/17.
 */
public class MarinaClass {
    private int count;
    private short number;


    public MarinaClass(int newCount, short newNumber) {
        count = newCount;
        number = newNumber;
    }

    public void setCount(int newCount) {
       count = newCount;
    }

    public int getCount() {
        return count;

    }

    public void setNumber(short newNumber) {
        number = newNumber;
    }

    public short getNumber() {
        return number;
    }

    public static void main(String[] args) {
        MarinaClass aClass1 = new MarinaClass(23, (short) 5);
        int a = aClass1.getNumber();

        System.out.println("Before " + a);


        aClass1.setNumber((short)3);

        a = aClass1.getNumber();

        System.out.println("After " + a);


    }
}
