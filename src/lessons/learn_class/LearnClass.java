package lessons.learn_class;

/**
 * Created by mskubit on 7/15/17.
 */
public class LearnClass {
    private int age;//field with name age
    private short countOfPeople;//field with name countOfPeople


    public LearnClass(int newAge, short newCountOfPeople) {
        if (newAge < 0) {
            age = 1;
        } else {
            age = newAge;

        }
        countOfPeople = newCountOfPeople;
    }

    public LearnClass() {
        age = 45;
        countOfPeople = 12;
    }

    //access_modificator return_type(or void) name_of_method(incoming parameters or not)
    public void setAge(int newAge) {
        age = newAge;
    }

    public int getAge(){
        return age;
    }
}
