package lessons.learn_string;

public class LearnString {
    public static void main(String[] args) {//array[5]
        String str = "Hello my young friend";
//        System.out.println(str.charAt(0));
        //String newStr = str + "World";
        //System.out.println(str.replaceAll("o", "t"));
        //System.out.println(str.substring(8));
        //System.out.println(str.lastIndexOf('o'));
        String str1 = "Hello";//literal
        String str2 = new String("Hello");//object

//        System.out.println(str1.equals(str2));
        /*System.out.println(str1 == str2.intern());
        System.out.println(str1 == str2);*/

        System.out.println("1" + 2 + 3);
        System.out.println(1 + 2 + "3");
    }
}
