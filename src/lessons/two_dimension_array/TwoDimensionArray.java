package lessons.two_dimension_array;

import java.util.Random;

/**
 * Created by mskubit on 6/18/17.
 */
public class TwoDimensionArray {
    public static void main(String[] args) {


        int[][] array1 = {
                {4,  5,  61},
                {41, 78, 7},
                {6,  15, 9}
        };

        int[][] array = new int[4][5];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(20);
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
