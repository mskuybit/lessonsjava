package lessons.bit_operation;

/**
 * Created by mskubit on 6/17/17.
 */
public class BitOperation {
    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(39));//00100111
        System.out.println(Integer.toBinaryString(17));//00010001
        //System.out.println(39 | 17);//                      00110111
        System.out.println(39 & 17);//                      00000001

        System.out.println("Result = " + Integer.toBinaryString(55));//00010001
    }
}

