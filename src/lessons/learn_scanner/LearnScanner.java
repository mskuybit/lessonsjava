package lessons.learn_scanner;

import java.util.Scanner;

public class LearnScanner {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please, type something");

        String result = scanner.nextLine();

        //int sum = Integer.parseInt(result);

        //int sum = scanner.nextInt();


        System.out.println("You've typed " + result);
    }
}