package lessons.learn_condition;

/**
 * Created by mskubit on 6/18/17.
 */
public class LearnCondition {
    public static void main (String[] args) {
        int a = 81;

        /*if (a < 10) {
            System.out.println("a < 10");
        } else if(a == 10) {
            System.out.println("a == 10");
        } else {
            System.out.println("a > 10");
        }*/

        //ternary operator
        // condition ? statement if condition true : statement if condition false

        /*int b = a < 10 ? a == 8 ? 20 : 50 : 25;
        System.out.println(b);*/

        if (a < 10 && a == 8) {
            //System.out.println("a < 10 and a == 8");
        }

        if (a == 10 || a == 8) {
            //System.out.println("a == 10 and a == 8");
        }

        if(a == 8){
            //System.out.println("a == 8");
        }

        if(a == 9){
            //System.out.println("a == 9");
        }

        if(a == 10){
            //System.out.println("a == 10");
        }

        switch (a) {
            case 8:{//case is analo if
                System.out.println("a == " + a);
                break;
            }
            case 9:{
                System.out.println("a == " + a);
                break;
            }
            case 10:{
                System.out.println("a == " + a);
                break;
            }

            default: {
                System.out.println("a == 528");
            }

        }
    }

}