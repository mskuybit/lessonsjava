package lessons.random_number;

import java.util.Random;

/**
 * Created by mskubit on 6/18/17.
 */
public class RandomNumber {
    public static void main(String[] args) {
        Random random = new Random();
        //System.out.println(random.nextInt(20)); (max - min) + min)

        int[] array = new int[10];

        for (int i = 0; i < array.length; ++i) {
            //array[i] = random.nextInt(30);
            array[i] = random.nextInt(20) + 30;
            System.out.print(array[i] + "\t");
        }
    }

}
