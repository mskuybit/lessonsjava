package lessons.learn_array;

/**
 * Created by mskubit on 6/17/17.
 */
public class LearnArray {
    public static void main (String[] args){
        int[] array = new int[6];//0

        array[0] = 13;
        array[1] = 17;
        array[2] = 11;
        array[3] = 10;
        array[4] = 3;

        int[] arr = {4,9,8};

        //System.out.println(arr[2]);

        //for, while, do-while, foreach(for)

        /*for (int i = 0; i < array.length; ++i) {
            //System.out.println(i);
            array[i] = i + 1;
        }*/

        /*int k = 0;

        while(k < array.length) {
            array[k] = k + 1;
            k++;
        }*/

        int j = 7;

        do {
           /* array[j] = j + 1;
            System.out.println(array[j]);
            ++j;   */
            System.out.println(j);

        } while(j < array.length);
    }

}
