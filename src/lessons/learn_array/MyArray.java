package lessons.learn_array;

/**
 * Created by mskubit on 6/19/17.
 */
public class MyArray {
    public static void main(String[] args) {

        int[] firstArray = new int[10];

        for (int i = 0; i < firstArray.length; i++) {
            firstArray[i] = i;

            System.out.print(firstArray[i] + "\t");
        }

        System.out.println();


        int[] secondArray = new int[20];

        for (int h = 0; h < secondArray.length; h++) {
            secondArray[h] = h;
            System.out.print(secondArray[h] + "\t");
        }

        System.out.println();

        int r = 0;
        while (r < firstArray.length) {
            firstArray[r] = r + 1;
            System.out.print(firstArray[r]);
            r++;
        }

        System.out.println();

        int k = 0;
        while (k < secondArray.length) {
            secondArray[k] = k + 1;
            System.out.print(secondArray[k] + "\t");
            k++;
        }

        System.out.println();

            int j = 0;

            do {
                firstArray[j] = j + 1;
                System.out.print(firstArray[j] + "\t");
                ++j;
            }
            while (j < firstArray.length);

        System.out.println();

            int s = 0;
            do {
                secondArray[s] = s + 1;
                System.out.print(secondArray[s] + "\t");
                ++s;
            }
            while (s < firstArray.length);

        }

    }