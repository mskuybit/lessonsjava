package homework.homeworksArrays;

import java.util.Random;

/**
 * Created by mskubit on 7/5/17.
 */
public class ArrayMax {
    public static void main(String[] args) {
        int array[][] = new int[5][10];

        Random random = new Random();

        for (int h = 0; h < array.length; h++) {
            for (int f = 0; f < array[h].length; f++) {
                array[h][f] = random.nextInt(50);
                System.out.print(array[h][f] + "\t");
            }

            System.out.println();
        }

        System.out.println();

        for (int h = 0; h < array.length; h++) {
            int max = array[h][0];
            for (int f = 0; f < array[h].length; f++) {

                if (array[h][f] > max) {
                    max = array[h][f];
                }

            }
            System.out.println("Max element of " + h + " row = " + max);
        }
    }
}











