package homework.homeworksArrays;

/**
 * Создать два массива из 20 чисел. Первый массив проинициализировать четными числами.
 Проинициализировать второй массив элементами первого массива при условии,
 что индекс делится на 4 без остатка и элемент больше 3, но меньше 16.
 Если условие не выполняется оставить элемент массива без изменения.
 */
public class WorkWithArrays {
    public static void main (String[] args) {
        int length = 20;

        int[] arrayOne = new int[length];
        int[] arrayTwo = new int[length];

        for (int a = 0; a < length; ++a) {
            arrayOne[a] = a * 2 ;
            System.out.print(arrayOne[a] + "\t");
            //0 1 2 3 4
            //0 2 4 6
        }

        System.out.println();

        for (int b = 0; b < length; ++b) {
            if(b % 4 == 0 && arrayOne[b] > 3 && arrayOne[b] < 16) {
                arrayTwo[b] = arrayOne[b];
            }
            System.out.print(arrayTwo[b] + "\t");
        }

            //arrayTwo[0] = arrayOne[1];



    }
}
