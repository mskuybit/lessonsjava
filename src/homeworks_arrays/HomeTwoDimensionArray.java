package homework.homeworksArrays;

import java.util.Random;

/**
 * Создать двумерный массив размерностью 5 * 6,
 * проинициализировать случайным числами.
 * Если элемент четный поменять на 99. Вывести в консоль.
 */
public class HomeTwoDimensionArray {
    public static void main(String[] args) {

        int[][] array2 = new int[5][6];

        Random random = new Random();

        for (int h = 0; h < array2.length; h++) {
            for (int f = 0; f < array2[h].length; f++) {
                array2[h][f] = random.nextInt(50);
                if (array2[h][f] % 2 == 0) {
                    array2[h][f] = 99;
                }
                System.out.print(array2[h][f] + "\t");
            }
            System.out.println();

        }
    }

}