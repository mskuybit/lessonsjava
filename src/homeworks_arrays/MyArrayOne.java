package homework.homeworksArrays;

/**
 * Created by mskubit on 6/20/17.
 */
public class MyArrayOne {
    public static void main(String[] args) {
        int length = 30;

        int[] arrayOne = new int[length];//0
        int[] arrayTwo = new int[length];//0

        for (int a = 0; a < length; ++a) {
            arrayOne[a] = a * 2 + 1;
            System.out.print(arrayOne[a] + "\t");
        }

        System.out.println();

        for (int b = 0; b < length; ++b) {
            if (b > 4 && b % 5 == 0 &&
                    ((arrayOne[b] > 0 && arrayOne[b] < 6) || (arrayOne[b] > 10 && arrayOne[b] < 20))) {//true && false &&
                // ((true && false) || (true && true))
                arrayTwo[b] = arrayOne[b];
            }

            System.out.print(arrayTwo[b] + "\t");
        }

    }
}
