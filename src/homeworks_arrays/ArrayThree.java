package homework.homeworksArrays;

import java.util.Random;

/**
 * Created by mskubit on 6/29/17.
 */
public class ArrayThree {
    public static void main(String[] args) {
        int[][] array = new int[5][6];

        int count = 0;

        Random random = new Random();

        for (int h = 0; h < array.length; h++) {
            for (int f = 0; f < array[h].length; f++) {
                array[h][f] = random.nextInt(90) + 10;
                System.out.print(array[h][f] + "\t");
            }
            System.out.println();
        }

        for (int h = 0; h < array.length; h++) {
            for (int f = 0; f < array[h].length; f++) {
                int element = array[h][f];

                int tens = element / 10;
                int ones = element - tens * 10;

                if ((tens + ones) % 2 == 0){
                    System.out.println("Number with % 2  = " + element);
                    count++;
                }

            }

        }

        System.out.println("Count of numbers = " + count);
    }
}




