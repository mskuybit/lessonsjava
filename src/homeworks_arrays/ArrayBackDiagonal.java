package homework.homeworksArrays;

import java.util.Random;

public class ArrayBackDiagonal {
    public static void main(String[] args) {

        int[][] array = new int[4][4];

        Random random = new Random();


        for (int h = 0; h < array.length; h++) {
            for (int f = 0; f < array.length; f++) {
                array[h][f] = random.nextInt(20);

                System.out.print(array[h][f] + "\t");

            }
            System.out.println();
        }

        int sum = 0;

        for (int h = 0; h < array.length; h++) {
            for (int f = 0; f < array.length; f++) {
                if(h + f == array.length - 1) {
                    sum += array[h][f];
                }
            }
        }

        System.out.println("Sum of element of back diagonal = " + sum);
    }
}

