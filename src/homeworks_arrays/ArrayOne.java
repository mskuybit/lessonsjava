package homework.homeworksArrays;

import java.util.Random;

/**
 * Created by mskubit on 6/28/17.
 */
public class ArrayOne {
    public static void main(String[] args) {

        int count = 0;

        Random random = new Random();

        int[] array = new int[12];

        for (int i = 0; i < array.length; i++) {//11
            array[i] = random.nextInt(31) - 15;//(max - min) + min)

            System.out.print(array[i] + "\t");
        }

        System.out.println();

        for (int i = 0; i < array.length - 1; i++) {//11

            int firstName = array[i];
            int secondName = array[i + 1];

            if(firstName % 2 == 0 && secondName % 2 == 0) {
                System.out.println(firstName + " : " + secondName);
                count++;
            }
        }

        System.out.println("Count of pairs = " + count);
    }
}