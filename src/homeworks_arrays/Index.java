package homework.homeworksArrays;

import java.util.Random;

/**
 * Created by mskubit on 7/15/17.
 */
public class Index {

    public static void main(String[] args) {
        int[][] array = new int[7][4];

        Random random = new Random();
        int max = 0;
        int index = 0;
        for (int a = 0; a < array.length; a++) {
            int m = 1;
            for (int b = 0; b < array[0].length; b++) {
                array[a][b] = random.nextInt(11) - 5;
                System.out.print(array[a][b] + "\t");
                m *= array[a][b];
            }
            System.out.println();
            if (Math.abs(max) < Math.abs(m)) {
                max = m;
                index = a;
            }

        }
        System.out.println(("Max abs is row with index " + index + " max: " + max));//выводим результат

    }
}



