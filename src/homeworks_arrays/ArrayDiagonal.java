package homework.homeworksArrays;

import java.util.Random;

/**
 * Created by mskubit on 7/8/17.
 */
public class ArrayDiagonal {
    public static void main(String[] args) {

        int[][] array = new int[4][4];

        Random random = new Random();


        for (int h = 0; h < array.length; h++) {
            for (int f = 0; f < array.length; f++) {
                array[h][f] = random.nextInt(20);

                System.out.print(array[h][f] + "\t");

            }
            System.out.println();
        }
        int sum = 0;
        for (int h = 0; h < array.length; h++) {
            for (int f = 0; f < array.length; f++) {
                if(h == f) {
                    sum += array[h][f];
                    //sum = sum + 1;
                }
            }
        }
        System.out.println("Sum of element of diagonal = " + sum);
    }
}

/*
*
5	2	2	0
0	1	5	6
4	2	2	0
4	2	2	0

5 - 0;0
1 - 1;1
2 - 2;2
0 - 3;3
* */
