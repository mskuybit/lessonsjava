package homework.homework_string;

public class StringLongAndShort {

    public static void main(String[] args) {

        /*String str = "Please, add money to your account to do withdraw.";
        String tmp = "";
        String longestWord = "";
        String shortWord = str;

        for (int i = 0; i <= str.length(); i++){

            if (i == str.length() || str.charAt(i) == ' ') {
                longestWord = longestWord.length() < tmp.length() ? tmp : longestWord;
                shortWord = shortWord.length() > tmp.length() ?  tmp : shortWord;

                tmp = "";
            }
            else tmp += str.charAt(i);
        }

        System.out.print("The longest word is \"" + longestWord + "\" The short word is \"" + shortWord + "\"");*/

        String[] strings = "Please, add money to your account to do withdraw.".split(" ");
        String maxWord = strings[0];
        String minWord = strings[0];


        for (String word: strings) {
            if (maxWord.length() < word.length()) {
                maxWord = word;
            }

            if (minWord.length() > word.length()) {
                minWord = word;
            }
        }


        for (String word: strings) {
            if(word.length() == maxWord.length()) {
                System.out.println("Max word = " + word);
            }

            if(word.length() == minWord.length()) {
                System.out.println("Min word = " + word);
            }

        }

        System.out.println("MaxWord = " + maxWord + " MinWord = " + minWord);

    }

}
