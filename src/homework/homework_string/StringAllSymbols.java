package homework.homework_string;

public class StringAllSymbols {
    public static void main(String[] args) {

        /*String str = "Please, add money to your account to do withdraw.";

        int count;

        for (char symbol: str.toCharArray()) {
            count = 0;
            for (char element: str.toCharArray()) {
                if (symbol == element) {
                    count++;
                }
            }
            System.out.println("Symbol " + symbol + " appears " + count + " times");
        }*/

        String myString = "Number oof symbolsss in the row.";
        String symbol;
        int count = 0;
        String temp;

        for (int j = 0; j < myString.length(); j++) {
            temp = myString;
            symbol = String.valueOf(myString.charAt(j));
            count = 0;
            for (int i = 0; i < myString.length(); i++) {
                if (!temp.contains(symbol)) {
                    break;
                }
                count++;
                int position = temp.indexOf(symbol);
                temp = temp.substring(position + 1);
            }
            myString = myString.replaceAll(symbol, " ");
            if (!symbol.equals(" ")) {
                System.out.println("Char " + symbol + " appears " + count + " times");
            }
        }

    }
}
