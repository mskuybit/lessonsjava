package homework.imitation__arraylist;


public class ImitationArrayList {
    private int[] array;//0

    public ImitationArrayList() {
        array = new int[10];
    }

    public void increaseArray(int size) {
        int[] temp = new int[array.length + size];

        for (int i = 0; i < array.length; i++) {
            temp[i] = array[i];
        }

        array = temp;
    }


    public void addArray(int[] newArray) {
        int countOfZeros = 0;
        int length = array.length;//10

        for (int temp : array) {
            if (temp == 0) {
                countOfZeros++;
            }
        }

        if (countOfZeros < newArray.length) {
            int[] temp = new int[array.length + newArray.length];//15

            for (int i = 0; i < array.length; i++) {
                temp[i] = array[i];
            }

            array = temp;
        }

        int indexZero = length - countOfZeros;


        for (int i = indexZero, j = 0; j < newArray.length; i++, ++j) {
            array[i] = newArray[j];
        }
    }

    public void increaseList(int length) {

        int[] temp = new int[array.length + length];

        for (int i = 0; i < array.length; i++) {
            temp[i] = array[i];
        }

        array = temp;
    }


    public void addElement(int element) {
        resize();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                array[i] = element;
                break;
            }
        }
    }

    private void resize() {
        //add logic for checking last element of array on 0
        int length = array.length;
        /*if(array[length - 1] != 0 ){

        }*/

        boolean shouldReSize = false;

        for(int element: array){
            if(element == 0) {
                shouldReSize = true;
            }
        }

        if(!shouldReSize){
            int[] temp = new int[length * 2];
            for (int i = 0; i < length; i++) {
                temp[i] = array[i];
            }

            array = temp;
        }

        //Arrays.sort(array);
    }

    public void changeElementByIndex(int index, int element) {
        array[index] = element;
    }


    public void removeElementByIndex(int index) {
        /*for (int i = 0; i < array.length; i++) {
            if(array[i] == array[index]){
                array[i] = 0;
            }
        }*/
        int[] temp = new int[array.length - 1];

        for (int i = 0; i < index; ++i) {
            temp[i] = array[i];
        }

        for (int i = index; i < temp.length; ++i) {
            temp[i] = array[i + 1];
        }

        array = temp;
    }


    public void printArrayByOrder() {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }

    }

    public void printArrayByReverse() {
        for (int i = array.length - 1; i >= 0; i--) {//9 -> 0
            System.out.print(array[i] + "\t");
        }
    }


    public void bubbleSort() {
        for (int i = array.length - 1; i > 0; i--) {    //5 1 6 4  - > 1 5 6 4
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }

// 2 3 5 1 3
    public void removeDuplicate() {
        /*for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    j--;
                }
            System.out.print(array[i] + "\t");
            }

        }*/


        for (int i = 0; i < array.length; i++) {
            int element = array[i];
            //int index = i;
//            boolean flag = false;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] == element) {
                    //flag = true;
                   // index = j;
                    removeElementByIndex(j);
                    //break;
                }
            }

           /* if (flag) {
                removeElementByIndex(index);
            }*/

        }
    }

    //45	2	30	15	29	3	9	8	7	30

    /*
    * int[] temp = new int[array.length - 1];

         for (int i = 0; i < index; ++i) {
            temp[i] = array[i];
        }

        for (int i = index; i < array.length && i < temp.length; ++i) {
                temp[i] = array[i + 1];
        }
        array = temp;
    * */

    public int[] getArray() {
        return array;
    }

    public static void main(String[] args) {

        ImitationArrayList list = new ImitationArrayList();

        list.addElement(45);
        list.addElement(2);
        list.addElement(30);
        list.addElement(15);
        list.addElement(15);
        list.addElement(29);
        list.addElement(30);

        //list.printArrayByOrder();

        list.addArray(new int[]{9, 8, 7});

        System.out.println();

        list.printArrayByOrder();

        //list.bubbleSort();

        //list.removeElementByIndex(2);

        System.out.println();

        list.removeDuplicate();

        list.printArrayByOrder();


        int[] array = {4, 5, 4, 8, 9};

        int index = 2;
    }


}












