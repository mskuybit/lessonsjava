package homeworks_classes;

/**
 * Created by mskubit on 7/20/17.
 */
public class NewOneCalculator {
    private int valueOne;
    private int valueTwo;
    private static int count = 0;

    public final int NUMBER;

    public static final int COUNT_PEOPLE = 8;

    {
        System.out.println("Non-static block initialization1");
    }

    static {
        //System.out.println("Static block initialization");
        System.out.println("Hello world");
    }

    {
        //System.out.println("Non-static block initialization2");
    }


    public NewOneCalculator(int newValueOne, int newValueTwo) {
        //System.out.println("Constructor");
        NUMBER = 7;
        valueOne = newValueOne;
        valueTwo = newValueTwo;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        NewOneCalculator.count = count;
    }

    public void setValueOne(int valueOne) {//(this, valueOne)
        this.valueOne = valueOne;
    }

    public void setValueTwo(int valueTwo) {
        this.valueTwo = valueTwo;
    }

    public void printSum() {
        System.out.println(valueTwo + valueOne);
    }

    public void printDif() {
        System.out.println(valueTwo - valueOne);
    }
    public void printMod() {
        System.out.println(valueTwo * valueOne);
    }
    public void printDiv() {
        System.out.println(valueOne / valueTwo);
    }

    public static void main(String[] args) {

    }


}

class TestCalculator {
    public static void main(String[] args) {

        int a = 7;

        NewOneCalculator nullReference = null;

        //nullReference = new NewCalculator(5, 4);

        if (a < 10) {
            nullReference = new NewOneCalculator(4, 4);
        } else {
            nullReference = new NewOneCalculator(8, 8);
        }

        NewOneCalculator calculator = new NewOneCalculator(100, 5);

        //NewCalculator.setCount(-5);

        NewOneCalculator calculator1 = new NewOneCalculator(100, 5);

        //System.out.println(NewCalculator.COUNT_PEOPLE);

        /*calculator.printSum();*/

        calculator.setValueOne(9);//(this, 9)
        calculator1.setValueTwo(3);

        /*calculator.printDif();

        calculator.printMod();

        calculator.printDiv();*/

    }
}









