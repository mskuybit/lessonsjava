package homeworks_classes;

public class BankAccount {
    private int balance;


    public BankAccount(int newBalance) {
        balance = newBalance;
    }

    public void printBalance() {
        System.out.println(balance);
    }

    public void withdraw(int amount) {
        if (balance < amount) {
            System.out.println("Please, add money to your account to do withdraw.");
        }
    }

    public void deposit(int amount) {
        balance += amount;
    }

    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount(200);
        bankAccount.withdraw(150);
        bankAccount.withdraw(60);
        bankAccount.deposit(100);
        bankAccount.printBalance();
    }
}



